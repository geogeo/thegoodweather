//
//  LocalStorageManager.swift
//  TheGoodWeather
//
//  Created by Geo on 16/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation
import CoreData

class LocalStorage {
    // MARK: - Properties
    
    static let sharedInstance = LocalStorage()
    private var timeFrames = [TimeFrame]()
    
    // MARK: - Initializers
    
    private init() {}
    
    // MARK: - Data getters
    
    /**
     Returns timeFrames array fetched from database.
    */
    func getLocalTimeFrames() -> [TimeFrame] {
        return timeFrames
    }
    
    // MARK: - Data loading
    
    /**
     Loads data from server to update database.
     Then load it from database.
     */
    func loadData(latitude: Float, longitude: Float, success: @escaping () -> Void, failure: @escaping (_: String) -> Void) {
        if (NetworkManager.sharedInstance.isReachable()) {
            InfoClimatService.sharedInstance.getDatasFromServer(latitude: latitude, longitude: longitude, completion: { (data) in
                if (data != nil) {
                    self.saveDataToDatabase(latitude: latitude, longitude: longitude, data: data!, completion: {
                        self.loadDataFromDatabase(latitude: latitude, longitude: longitude, success: {
                            success()
                        }, failure: {
                            failure("Couldn't fetch data.")
                        })
                    })
                } else {
                    self.loadDataFromDatabase(latitude: latitude, longitude: longitude, success: {
                        success()
                    }, failure: {
                        failure("Couldn't fetch data.")
                    })
                }
            })
        } else {
            loadDataFromDatabase(latitude: latitude, longitude: longitude, success: {
                success()
            }, failure: {
                failure("Couldn't fetch data.")
            })
        }
    }
    
    // MARK: - Database interractions
    
    /**
     Iterate through json data to extract timeframes for creation.
     */
    private func saveDataToDatabase(latitude: Float, longitude: Float, data: [String: Any], completion: @escaping () -> Void) -> Void {
        // Parse and save data
        for (key, value) in data {
            if key == "request_state" || key == "request_key" || key == "message" || key == "model_run" || key == "source" {
                continue
            }
            addOrUpdateTimeFrame(latitude: latitude, longitude: longitude, data: value as! [String: Any], key: key)
        }
        completion()
    }
    
    /**
     Creates or update timeFrame entity in database.
     In a further version we would check for update before creating.
    */
    private func addOrUpdateTimeFrame(latitude: Float, longitude: Float, data: [String: Any], key: String) -> Void {
        let managedObjectContext = persistentContainer.viewContext
        let windDict = data["vent_moyen"] as! [String: Float]
        let temperatureDict = data["temperature"] as! [String: Float]
        let humidityDict = data["humidite"] as! [String: Float]
        var timeFrame = entity(latitude: latitude, longitude: longitude, key: key)
        
        if timeFrame == nil {
            timeFrame = (NSEntityDescription.insertNewObject(forEntityName: "TimeFrame", into: managedObjectContext) as! TimeFrame)
            timeFrame!.latitude = latitude
            timeFrame!.longitude = longitude
            timeFrame!.date = key.toDate()
        }
        timeFrame!.averageWind = windDict["10m"]!
        timeFrame!.temperature = temperatureDict["sol"]!
        timeFrame!.humidity = humidityDict["2m"]!
        
        do {
            try managedObjectContext.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    /**
     Fetch timeframe for given latitude, longitude and date.
    */
    private func entity(latitude: Float, longitude: Float, key: String) -> TimeFrame? {
        let managedObjectContext = persistentContainer.viewContext
        let timeFrameFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "TimeFrame")
        let date = key.toDate()
        let predicate = NSPredicate(format: "(latitude == %f) AND (longitude == %f) AND (date == %@)", latitude, longitude, date! as NSDate)
        
        timeFrameFetch.predicate = predicate
        do {
            let result = try managedObjectContext.fetch(timeFrameFetch) as! [TimeFrame]
            if (result.count > 0) {
                return result[0]
            } else {
                return nil
            }
        } catch {
            fatalError("Failure to fetch results: \(error)")
        }
    }
    
    /**
     Fetch timeframes from database matching latitude and longitude.
     */
    private func loadDataFromDatabase(latitude: Float, longitude: Float, success: @escaping () -> Void, failure: @escaping () -> Void) -> Void {
        let managedObjectContext = persistentContainer.viewContext
        let timeFrameFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "TimeFrame")
        let coordinatesPredicate = NSPredicate(format: "(latitude == %f) AND (longitude == %f)", latitude, longitude)
        let dateSortDescriptor = NSSortDescriptor(key: #keyPath(TimeFrame.date), ascending: true)
        
        timeFrameFetch.predicate = coordinatesPredicate
        timeFrameFetch.sortDescriptors = [dateSortDescriptor]
        do {
            timeFrames = try managedObjectContext.fetch(timeFrameFetch) as! [TimeFrame]
        } catch {
            fatalError("Failure to fetch results: \(error)")
        }
        success()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TheGoodWeather")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
