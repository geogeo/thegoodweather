//
//  NetworkManager.swift
//  TheGoodWeather
//
//  Created by Geo on 16/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    // MARK: - Properties
    
    static let sharedInstance = NetworkManager()
    private let reachabilityManager = NetworkReachabilityManager(host: "www.google.com")
    
    // MARK: - Initializers
    
    /**
     Private init to make sure that the singleton can't be initialized by outside objects.
     The reachabilityManager is activated for listening.
     */
    private init() {
        startListening()
    }
    
    // MARK: - ReachabilityManager
    
    /**
     The reachabilityManager starts listening for reachability changes.
    */
    private func startListening() -> Void {
        reachabilityManager?.startListening()
    }
    
    /**
     Returns a boolean indicating if the network is reachable.
     If the reachabilityManager is nil, we will consider that the network is not reachable and return false.
     */
    func isReachable() -> Bool {
        if (reachabilityManager != nil) {
            return reachabilityManager!.isReachable
        }
        return false
    }
}
