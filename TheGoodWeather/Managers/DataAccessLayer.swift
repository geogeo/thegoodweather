//
//  DataAccessLayer.swift
//  TheGoodWeather
//
//  Created by Geo on 16/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation

class DataAccessLayer {
    // MARK: - Properties

    static let sharedInstance = DataAccessLayer()
    private var latitude: Float = 48.858053
    private var longitude: Float = 2.294289

    // MARK: - Initializers
    
    /**
     Private init to make sure that the singleton can't be initialized by outside objects.
     */
    private init() {}
    
    // MARK: - Setters
    
    func setCoordinates(latitude: Float, longitude: Float) -> Void {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    // MARK: - LocalStorage interractions
    
    /**
     Load data from storage for the registered latitude and longitude.
     If an error occurs, calls failure closure with given error message.
    */
    func loadData(success: @escaping () -> Void, failure: @escaping (_: String) -> Void) {
        LocalStorage.sharedInstance.loadData(latitude: latitude, longitude: longitude, success: {
            success()
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
    
    /**
     Return timeframes from localStorage.
     */
    func getTimeFrames() -> [TimeFrame] {
        return LocalStorage.sharedInstance.getLocalTimeFrames()
    }
}
