//
//  InfoClimatService.swift
//  TheGoodWeather
//
//  Created by Geo on 17/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation
import Alamofire

class InfoClimatService {
    // MARK: - Properties
    
    static let sharedInstance = InfoClimatService()
    private let jsonEndpoint: String! = "http://www.infoclimat.fr/public-api/gfs/json"
    private let authKey: String! = "AxkAFwJ8U3FQfQYxAnQDKlA4BDEBdwkuBXkAY1w5Uy5TOFc2UTEGYAVrB3pSfVZgVXhXNAw3VGQCaVIqAHJUNQNpAGwCaVM0UD8GYwItAyhQfgRlASEJLgVvAGdcL1MzUy5XNVE1BnoFaAdmUmBWfVV5VzYMNlRtAmJSMABvVDIDaQBkAmRTLlAgBmECYwM0UGUEbQE8CTgFZwBnXGRTOVM4VzBRMgZ6BWMHZVJhVmVVZ1czDDVUbgJ%2BUioAFFREA30AJAIjU2RQeQZ5AmcDaVA3"
    private let checksum: String! = "799eaac21dab538b78c73cbe06225d58"
    
    // MARK: - Initializers
    
    /**
     Private init to make sure that the singleton can't be initialized by outside objects.
     */
    private init() {}
    
    // MARK: - Requests
    
    /**
     Performs .GET request to jsonEndpoint with appropriates params.
     In a further version of this app, we would create proper error handling with enum, logging ... (instead of "REQUEST FAILED", "NO DATA" ).
     */
    func getDatasFromServer(latitude: Float, longitude: Float, completion: @escaping (_: [String: Any]?) -> Void) -> Void {
        request(jsonEndpoint, method: .get, parameters: parameters(latitude: latitude, longitude: longitude), encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            guard response.result.isSuccess == true else {
                completion(nil)
                return
            }
            guard let json = response.result.value as! [String: Any]?, let requestState = json["request_state"] as! Int?, requestState != 509 && requestState != 400 && requestState != 409 else {
                completion(nil)
                return
            }
            completion(json)
        }
    }
    
    // MARK: - Tools
    
    /**
     Returns full Parameters for the given latitude and longitude.
     Note that we replace percent encoding characters by UTF-8 ones so it can be reencoded properly during the request.
     */
    private func parameters(latitude: Float, longitude: Float) -> Parameters {
        let llParameter = "\(latitude),\(longitude)"
        let authEncoded: String! = authKey.removingPercentEncoding
        let parameters: Parameters = ["_ll": llParameter, "_auth": authEncoded, "_c": checksum]
        
        return parameters
    }
}
