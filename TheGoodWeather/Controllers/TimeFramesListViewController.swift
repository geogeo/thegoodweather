//
//  TimeFramesListViewController.swift
//  TheGoodWeather
//
//  Created by Geo on 16/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import UIKit

class TimeFramesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - Properties
    private var timeFrames = [TimeFrame]()
    private var selectedTimeFrame: TimeFrame? = nil
    
    // MARK: ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        timeFrames = DataAccessLayer.sharedInstance.getTimeFrames()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    
    /**
     Number of sections in tableview.
     Just 1 is needed in this case.
    */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    /**
     Number of rows per section in tableview.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeFrames.count
    }
    
    /**
     Set up cell with proper timeframe datas.
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeFrameCell", for: indexPath)
        let timeFrame = timeFrames[indexPath.row]
        let timeLabel = cell.viewWithTag(1) as! UILabel
        let temperatureLabel = cell.viewWithTag(2) as! UILabel
        
        timeLabel.text = timeFrame.date?.toString()
        temperatureLabel.text =  String(format: "%.1f °C", kelvinToCelsius(kelvin: timeFrame.temperature))
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTimeFrame = timeFrames[indexPath.row]
        performSegue(withIdentifier: "timeFrameListVCToTimeFrameDetailVC", sender: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "timeFrameListVCToTimeFrameDetailVC") {
            guard let _ = selectedTimeFrame else {
                return
            }
            let destinationController = segue.destination as! TimeFrameDetailViewController
            
            destinationController.timeFrame = selectedTimeFrame!
        }
    }
}
