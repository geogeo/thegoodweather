//
//  LaunchViewController.swift
//  TheGoodWeather
//
//  Created by Geo on 16/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import UIKit
import CoreLocation

class LaunchViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: - Properties
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    let locationManager = CLLocationManager()
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLocation()
    }
    
    // MARK: - UI
    
    /**
     Starts animating the spinner.
     */
    private func startSpinner() -> Void {
        spinner.startAnimating()
    }
    
    /**
     Stops animating the spinner.
     */
    private func stopSpinner() -> Void {
        spinner.stopAnimating()
    }
    
    // MARK: - GeoLocation
    
    /**
     Ask user for the permission to track his location.
     Set up location manager and start monitoring.
     */
    private func startLocation() -> Void {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    /**
     Called when location changes.
     Update location in DataAccessLayer if any coordinates are recorded.
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let firstLocation = locations.first {
            let coordinates = firstLocation.coordinate
            let latitude = Float(coordinates.latitude)
            let longitude = Float(coordinates.longitude)
            
            DataAccessLayer.sharedInstance.setCoordinates(latitude: latitude, longitude: longitude)
            locationManager.stopUpdatingLocation()
        }
    }
    
    // MARK: - Events
    
    /**
    Button pression triggers data loading.
    */
    @IBAction func didPressLaunchButton(_ sender: UIButton) {
        locationManager.stopUpdatingLocation()
        startSpinner()
        loadData()
    }    
    
    // MARK: - DataLoading
    
    /**
     Call DataAccessLayer to initiate data loading.
     If the loading is a success, perform segue to the TimeFramesViewController.
     Else an error message is displayed to the user. It is up to the user to kill the app.
     */
    private func loadData() -> Void {
        DataAccessLayer.sharedInstance.loadData(success: {
            self.stopSpinner()
            self.presentNextViewController()
        }) { (errorMessage) in
            let alertController = UIAlertController(title: "An error occured", message: "\(errorMessage) Please try to relaunch the app.", preferredStyle: .alert)
            
            self.stopSpinner()
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Navigation
    
    /**
     Performs segue to the TImeFramesViewController.
     */
    private func presentNextViewController() -> Void {
        self.performSegue(withIdentifier: "launchVCToTimeFramesListVC", sender: nil)
    }
}
