//
//  TimeFrameDetailViewController.swift
//  TheGoodWeather
//
//  Created by Geo on 18/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import UIKit

class TimeFrameDetailViewController: UIViewController {
    // MARK: - Properties
    
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var averageWindLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    var timeFrame: TimeFrame?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // MARK: - UI
    
    /**
     Update UILabels with correct timeframe values.
     */
    private func setUpLabels() -> Void {
        guard let _ = timeFrame else {
            return
        }
        humidityLabel.text = String(format: "%.1f %%", timeFrame!.humidity)
        temperatureLabel.text = String(format: "%.1f °C", kelvinToCelsius(kelvin: timeFrame!.temperature))
        averageWindLabel.text = String(format: "%.1f km/h", timeFrame!.averageWind)
    }
    
    // MARK: - Events
    
    @IBAction func didPressBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
