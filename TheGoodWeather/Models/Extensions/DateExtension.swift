//
//  DateExtension.swift
//  TheGoodWeather
//
//  Created by Geo on 18/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation

extension Date {
    /**
     Returns string from date.
     */
    func toString() -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: self)
    }
}
