//
//  StringExtensions.swift
//  TheGoodWeather
//
//  Created by Geo on 17/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation

extension String {
    /**
     Returns date from string.
    */
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)
    }
}
