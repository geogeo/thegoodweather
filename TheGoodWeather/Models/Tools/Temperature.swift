//
//  Temperature.swift
//  TheGoodWeather
//
//  Created by Geo on 18/12/2017.
//  Copyright © 2017 GBaeONE. All rights reserved.
//

import Foundation

/**
 Convert Kevlin to Celsius.
 */
func kelvinToCelsius(kelvin: Float) -> Float {
    return kelvin - 273.5
}


/**
 Convert Celsius to Kelvin.
 */
func celsiusToKelvin(celsius: Float) -> Float {
    return celsius + 273.5
}
